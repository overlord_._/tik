"""
    Burrows-Wheeler + LZW
"""


def binary_search(list_to_search, value):
    """
    Search value in list
    :param list_to_search:
    :param value:
    :return:
    """
    start = 0
    end = len(list_to_search) - 1

    while start <= end:
        mid = (start + end) // 2
        if list_to_search[mid] > value:
            end = mid - 1
        elif list_to_search[mid] < value:
            start = mid + 1
        else:
            return mid


def BWT(input_string):
    """
    BWT algorithm compress
    :param input_string:
    :return:
    """
    rotation_table = [
        input_string[index:] + input_string[:index] for index in range(len(input_string))
    ]
    sorted_table = sorted(rotation_table)
    last_letter = [row[-1] for row in sorted_table]
    BW_output = "".join(last_letter)
    input_str_index = binary_search(sorted_table, input_string)
    return BW_output, input_str_index


def LZW_compress(input_string):
    """
    Compress string to sequence of codes
    :param input_string:
    :return:
    """

    # init dictionary
    dict_size = 96
    __dict = {chr(i + 32): i for i in range(dict_size)}

    worked_str = ""
    LZW_output = []
    for char in input_string:
        working_str = worked_str + char
        if working_str in __dict:
            worked_str = working_str
        else:
            # add code to output
            LZW_output.append(__dict[worked_str])
            # add new code to the dictionary.
            __dict[working_str] = dict_size
            dict_size += 1
            worked_str = char

    # code for worked_str
    LZW_output.append(__dict[worked_str])
    return LZW_output


if __name__ == '__main__':
    string_to_encode = open("encode_input.txt", "r", encoding="utf-8").read()
    BW_out, BW_index = BWT(string_to_encode)
    LZW_out = LZW_compress(BW_out)
    LZW_out_str = ""
    for num in LZW_out:
        LZW_out_str += bin(num)[2:].zfill(8)
    with open("encode_output.txt", "w") as output_file:
        output_file.write(f"{LZW_out_str}\n{BW_index + 1}")
