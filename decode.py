"""
    Huffman algorithm
"""


class NodeTree:
    def __init__(self, left=None, right=None):
        self.left = left
        self.right = right


def huffman_code_tree(node, binString=''):
    """
    make Huffman code
    :param node:
    :param binString:
    :return:
    """
    if type(node) is str:
        return {node: binString}
    l, r = node.left, node.right
    d = dict()
    d.update(huffman_code_tree(l, binString + '0'))
    d.update(huffman_code_tree(r, binString + '1'))
    return d


def make_tree(nodes):
    """
    create tree
    :param nodes:
    :return:
    """
    while len(nodes) > 1:
        (key1, c1) = nodes[-1]
        (key2, c2) = nodes[-2]
        nodes = nodes[:-2]
        node = NodeTree(key1, key2)
        nodes.append((node, c1 + c2))
        nodes = sorted(nodes, key=lambda x: x[1], reverse=True)
    return nodes[0][0]


def huffman_decode(encoded_data, codes):
    decoded_output  = ""
    code_str = ""

    for symbol in encoded_data:
        code_str += symbol
        possible_code = codes.get(code_str, None)
        if possible_code is not None:
            decoded_output  += possible_code
            code_str = ""

    return decoded_output


if __name__ == '__main__':
    input_file = open("decode_input.txt", "r")
    string_to_decode = input_file.readline().strip()
    alphabet = input_file.readline().strip().split()
    probs = input_file.readline().strip().split()

    data = []
    for i in range(len(alphabet)):
        data.append((alphabet[i], int(probs[i])))
    data = sorted(data, key=lambda x: x[1], reverse=True)

    encoding = huffman_code_tree(make_tree(data))
    encoding = dict((v, k) for k, v in encoding.items())

    open("decode_output.txt", "w").write(huffman_decode(string_to_decode, encoding))
